<?php

/**
 * +----------------------------------------------------------------------
 * | 润憬商城系统 [ 高性价比的通用商城系统 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2022~2023 https: *www.honc.fun All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
 * +----------------------------------------------------------------------
 * | Author: 润憬科技 Hon(陈烁临) <2275604210@qq.com>
 * +----------------------------------------------------------------------
 */

namespace process;

use Webman\Event\Event;
use Workerman\Crontab\Crontab;


class TimerTask
{
  public static function onWorkerStart()
  {
    // 每秒钟执行一次
    // new Crontab('*/1 * * * * *', function () {
    //   echo date('Y-m-d H:i:s') . "\n";
    // });

    // // 每5秒执行一次
    // new Crontab('*/5 * * * * *', function () {
    //   echo date('Y-m-d H:i:s') . "\n";
    // });

    // // 每分钟执行一次
    // new Crontab('0 */1 * * * *', function () {
    //   echo date('Y-m-d H:i:s') . "\n";
    // });
    $m = env('TIMER_MINUTES', 3);
    // 每3分钟执行一次
    new Crontab("0 */{$m} * * * *", function () {
      echo date('Y-m-d H:i:s') . "\n";
      echo 'timer...' . PHP_EOL;
      Event::emit('StoreTask', null);
    });
  }
}
