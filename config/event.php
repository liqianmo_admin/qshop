<?php

use app\common\event\OrderPaySuccess;

return [
  'OrderPaySuccess' => [
    [OrderPaySuccess::class, 'handle']
  ],
  // 定时任务：商城模块
  'StoreTask' => [\app\timer\controller\Store::class, 'handle'],

  // 定时任务：商城订单
  'Order' => [\app\timer\controller\Order::class, 'handle'],

  // 定时任务：用户优惠券
  'UserCoupon' => [\app\timer\controller\UserCoupon::class, 'handle'],

  // 定时任务：会员等级
  'UserGrade' => [\app\timer\controller\UserGrade::class, 'handle'],
];
