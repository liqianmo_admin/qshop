APP_NAME = webman

APP_VERSION = '0.1.0'

APP_DEBUG = false

DEFAULT_TIMEZONE = "Asia/Shanghai"

# WORKMAN 服务进程配置
WORKMAN_LISTEN = "http://0.0.0.0:8787" #服务域名端口
WORKMAN_COUNT = 16 #进程数


# 数据库配置相关
MYSQL_DB_HOST = ~db_host~
MYSQL_DB_PORT = ~db_port~
MYSQL_DB_NAME = ~db_name~
MYSQL_DB_PREFIX = qshop_
MYSQL_DB_USER = ~db_user~
MYSQL_DB_PASSWORD = ~db_pwd~

#redis  
REDIS_HOST = 127.0.0.1
REDIS_PASSWORD = 1234567
REDIS_PORT = 6379
#默认db
REDIS_DB_DEFAULT = 10

