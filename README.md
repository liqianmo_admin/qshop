# Webman 轻量商城开源版

#### 项目介绍

Webman 轻量商城是基于萤火商城 2.0 开源版适配 webman 修改成的，是全新推出的一款轻量级、高性能、前后端分离的电商系统，支持微信小程序 + H5+ 公众号 + APP，前后端源码完全开源，看见及所得，完美支持二次开发，可学习可商用，让您快速搭建个性化独立商城。

萤火商城 2.0 使用的是 ThinkPHP6.0 框架，本项目移植使用 webman 框架，快速适配完美支持。适配过程中主要对请求，事件，定时任务等做兼容处理，如果您想让 Thinkphp 项目更高效，正在纠结是否把 Thinkphp 换成 webman，本项目就是很好的答案，别犹豫马上开始吧！

    如果对您有帮助，您可以点右上角 “Star” 收藏一下 ，获取第一时间更新，谢谢！

#### 技术特点

- 前后端完全分离 (互不依赖 开发效率高)
- 采用 PHP8.0 (强类型严格模式)
- Webman（基于 workerman 开发的超高性能 PHP 框架）
- Uni-APP（开发跨平台应用的前端框架）
- Ant Design Vue（企业级中后台产品 UI 组件库）
- RBAC（基于角色的权限控制管理）
- Composer 一键引入三方扩展
- 部署运行的项目体积仅 30 多 MB（真正的轻量化）
- 所有端代码开源 (服务端 PHP、后台 vue 端、uniapp 端)
- 简约高效的编码风格 (可能是最适合二开的源码)
- 源码中清晰中文注释 (小白也能看懂的代码)
- ThinkPHP6 项目完美移植 Webman 性能提升 10 倍

#### 页面展示

![前端展示](https://gitee.com/hon-csl/qshop/raw/master/data/qdzs.png "前端展示.png")
![后台-首页](https://gitee.com/hon-csl/qshop/raw/master/data/htsy.png "后台-首页.png")
![后台-页面设计](https://gitee.com/hon-csl/qshop/raw/master/data/htdmsj.png "后台-页面设计.png")
![后台-编辑商品](https://gitee.com/hon-csl/qshop/raw/master/data/spbj.png "后台-编辑商品.png")

#### 系统演示

> 由于本人暂无公网服务器，演示效果同萤火商城 2.0 效果

- 商城后台演示：https://shop2.yiovo.com/admin/
- 用户名和密码：admin yinghuo
![前端演示二维码](https://images.gitee.com/uploads/images/2021/0316/104516_3778337e_2166072.png "111.png")
<!-- - QQ交流群 806461900 -->

#### 源码下载

1. 主商城端（又称后端、服务端，PHP 开发 用于管理后台和提供 api 接口）

   下载地址：https://gitee.com/hon-csl/qshop

2. 用户端（也叫客户端、前端，uniapp 开发 用于生成 H5 和微信小程序）

   下载地址：https://gitee.com/hon-csl/qshop-uniapp

3. 后台 VUE 端（指的是商城后台的前端代码，使用 vue2 编写，分 store 模块和 admin 模块）

   下载地址：https://gitee.com/hon-csl/qshop-store

   下载地址：https://gitee.com/hon-csl/qshop-admin

#### 代码风格

- PHP8 强类型严格模式
- 严格遵守 MVC 设计模式 同时具有 service 层和枚举类 enum 支持
- 简约整洁的编码风格 绝不冗余一行代码
- 代码注释完整易读性高 尽量保障初级程序员也可读懂 极大提升二开效率
- 不允许直接调用和使用 DB 类（破坏封装性）
- 不允许使用原生 SQL 语句 全部使用链式操作（可维护性强）
- 不允许存在复杂 SQL 查询语句（可维护性强）
- 所有的 CURD 操作均通过 ORM 模型类 并封装方法（扩展性强）
- 数据库设计满足第三范式
- 前端 JS 编码均采用 ES6 标准

#### 环境要求

- CentOS 7.0+
- Nginx 1.10+
- PHP 8.0
- MySQL 5.6+

#### 如何安装

<!-- ##### 一、自动安装（推荐）

1. 将后端源码上传至服务器站点，并且将站点运行目录设置为/public
2. 在浏览器中输入站点域名 + /install，例如：https://www.你的域名.com/install
3. 根据页面提示，自动完成安装即可 -->

##### 安装

1. 将后端源码上传至服务器站点，并且将站点运行目录设置为/public
2. 创建一个数据库，例如：qshop_db
3. 导入数据库表结构文件，路径：/data/cteate_table.sql
4. 导入数据库默认数据文件，路径：/data/init_data.sql
5. 修改数据库连接文件，将数据库用户名密码等信息填写完整，路径/.env
6. 在站点根目录执行 php start.php start -d

#### 后台地址

- 超管后台：https://www.你的域名.com/admin
- 商户后台：https://www.你的域名.com/store
- 默认的账户密码：admin 123456

#### 定时任务

用于自动处理订单状态、优惠券状态、会员等级等

```sh
.env 文件中配置调度时长即可

TIMER_MINUTES = 3
```

#### 安全&缺陷

如果您碰到安装和使用问题可以查阅[Issue](https://gitee.com/hon-csl/qshop/issues?state=all)，或者将操作流程和截图详细发出来，我们看到后会给出解决方案。

如果有 BUG 或者安全问题，我会第一时间修复。

#### 版权须知

1. 允许个人学习研究使用，支持二次开发，允许商业用途（仅限自运营）。
2. 允许商业用途，但仅限自运营，如果商用必须保留版权信息，望自觉遵守。
3. 不允许对程序代码以任何形式任何目的的再发行或出售，否则将追究侵权者法律责任。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有 Copyright © 2023 By 陈烁临 （润憬科技） All rights reserved。
