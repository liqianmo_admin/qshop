<?php
// +----------------------------------------------------------------------
// | 萤火商城系统 [ 致力于通过产品和服务，帮助商家高效化开拓市场 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2023 https://www.yiovo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 萤火科技 <admin@yiovo.com>
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace cores\middleware;



use app\common\library\Log;
use support\Request;
use support\Response;

/**
 * 中间件：应用日志
 */
class AppLog
{
    // 访问日志
    private static $beginLog = '';

    /**
     * 前置中间件
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, \Closure $next)
    {
        // 记录访问日志
        if (env('begin_log')) {
            $log = $this->getVisitor($request);
            $log .= "\r\n" . '[ header ] ' . print_r($request->header(), true);
            $log .= "" . '[ param ] ' . print_r($request->all(), true);
            $log .= '--------------------------------------------------------------------------------------------';
            static::$beginLog = $log;
        }
        return $next($request);
    }

    /**
     * 记录访问日志
     * @param Response $response
     */
    public function end(Response $response)
    {
        Log::record(static::$beginLog . 'begin', 'info');
        Log::end();
    }

    /**
     * 获取请求路径信息
     * @param Request $request
     * @return string
     */
    private function getVisitor(Request $request): string
    {
        $data = [$request->getRealIp(), $request->method(), $request->url()];
        return implode(' ', $data);
    }
}
